import Head from "next/head";
import React, { ReactNode } from "react";
import Navbar from "../ui/Navbar";

interface Props {
  children: ReactNode;
  title?: string;
}

const HOST = typeof window !== "undefined" ? window.location : "";

export const Layout = ({ children, title }: Props) => {
  return (
    <>
      <Head>
        <title>{title ?? "Pokemon App"}</title>
        <meta name="author" content="emm3000" />
        <meta
          name="description"
          content={`Informacion sobre el pokemon ${title}`}
        />
        <meta name="keywords" content={`${title} , pokemon, pokedex`} />

        <meta property="og:title" content={`Información sobre ${title}`} />
        <meta
          property="og:description"
          content={`Esta es la página sobre ${title}`}
        />
        <meta property="og:image" content={`${HOST}/img/banner.png`} />
      </Head>
      <Navbar />
      <main
        style={{
          padding: "10px 20px",
        }}
      >
        {children}
      </main>
    </>
  );
};
