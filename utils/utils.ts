import { SmallPokemon } from "../interfaces";
import { PopularResponse } from "../interfaces/pokemon-full";
import { PropsMapped } from "../interfaces/pokemonProps";

export const POKEMON_KEY = "POKEMON_KEY";

export function getUrlImage(id: number) {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${id}.svg`;
}

export function pokemonResponseToUIProps(
  response: PopularResponse
): PropsMapped {
  return {
    name: response.name,
    imgBack: response.sprites.back_default,
    imgFront: response.sprites.front_default,
    pokemonImg: response.sprites.other?.dream_world.front_default ?? "",
    imgShintyBack: response.sprites.back_shiny,
    imgShinyFront: response.sprites.front_shiny,
    id: String(response.id),
  };
}

export function saveLocalStorage(value: PropsMapped): void {
  if (typeof window !== "undefined") {
    const favorites: PropsMapped[] = JSON.parse(
      localStorage.getItem(POKEMON_KEY) ?? "[]"
    ) as PropsMapped[];

    if (isFavorite(value.id)) return;

    favorites.push(value);
    const jsonToString = JSON.stringify(favorites);
    localStorage.setItem(POKEMON_KEY, jsonToString);
  }
}

export function getAllFavorites(): PropsMapped[] {
  if (typeof window !== "undefined") {
    return JSON.parse(
      localStorage.getItem(POKEMON_KEY) ?? "[]"
    ) as PropsMapped[];
  }
  return [];
}

export function deleteFavorite(pokemonId: string) {
  if (typeof window !== "undefined") {
    const allFavorites = JSON.parse(
      localStorage.getItem(POKEMON_KEY) ?? "[]"
    ) as PropsMapped[];
    const deletedItem = allFavorites.filter(
      (pokemon) => pokemon.id !== pokemonId
    );
    const jsonToString = JSON.stringify(deletedItem);
    localStorage.setItem(POKEMON_KEY, jsonToString);
  }
}

export function isFavorite(pokemonId: string): boolean {
  if (typeof window !== "undefined") {
    const favorites: PropsMapped[] = JSON.parse(
      localStorage.getItem(POKEMON_KEY) ?? "[]"
    ) as PropsMapped[];
    return Boolean(favorites.find((pokemon) => pokemon.id === pokemonId));
  }
  return false;
}

export function safeLocalStorage(predicate: () => boolean): boolean {
  if (typeof window != "undefined") {
    return predicate();
  }
  return false;
}
