export interface PropsMapped {
  pokemonImg: string;
  name: string;
  imgFront: string;
  imgBack: string;
  imgShinyFront: string;
  imgShintyBack: string;
  id: string;
}
