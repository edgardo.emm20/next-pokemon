import { Button, Card, Container, Grid, Image, Text } from "@nextui-org/react";
import { GetStaticPaths, GetStaticProps } from "next";
import { Layout } from "../../components/layouts";
import { NextPage } from "next";
import { pokeApi } from "../../api";
import { PopularResponse } from "../../interfaces/pokemon-full";
import { pokemonResponseToUIProps } from "../../utils";
import { useEffect } from "react";
import confetti from "canvas-confetti";
import { PropsMapped } from "../../interfaces/pokemonProps";
import { usePokemonLocalStorage } from "../../hooks";
import { PokemonListResponse } from "../../interfaces";

export const PokemonByNamePage: NextPage<PropsMapped> = ({
  imgBack,
  imgFront,
  imgShintyBack,
  imgShinyFront,
  name,
  pokemonImg,
  id,
}) => {
  const { toggle, isFavoriteState } = usePokemonLocalStorage({
    imgBack,
    imgFront,
    imgShintyBack,
    imgShinyFront,
    name,
    pokemonImg,
    id,
  });

  useEffect(() => {
    if (isFavoriteState)
      confetti({
        zIndex: 999,
        particleCount: 100,
        spread: 160,
        angle: -100,
        origin: {
          x: 1,
          y: 0,
        },
      });
  }, [isFavoriteState]);

  return (
    <Layout title={name}>
      <Grid.Container
        css={{
          marginTop: "5px",
        }}
        gap={2}
      >
        <Grid xs={12} sm={4}>
          <Card
            isHoverable
            isPressable
            css={{
              padding: "30px",
            }}
          >
            <Card.Body>
              <Card.Image
                src={pokemonImg || "/no-image.png"}
                alt={name}
                width="100%"
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>
        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Text h1 transform="capitalize">
                {name}
              </Text>
              <Button color="gradient" onPress={toggle}>
                Guardar en favoritos {!isFavoriteState || `❤`}
              </Button>
            </Card.Header>
            <Card.Body>
              <Text size={30}>Sprites:</Text>
              <Container direction="row" display="flex" gap={0}>
                <Image src={imgFront} alt={name} width={100} height={100} />
                <Image src={imgBack} alt={name} width={100} height={100} />
                <Image
                  src={imgShinyFront}
                  alt={name}
                  width={100}
                  height={100}
                />
                <Image
                  src={imgShintyBack}
                  alt={name}
                  width={100}
                  height={100}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  const { data } = await pokeApi.get<PokemonListResponse>("/pokemon?limit=151");
  const params = data.results.map((pokemon) => {
    return { params: { name: pokemon.name } };
  });
  return {
    paths: params,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { name } = ctx.params as { name: string };
  const { data } = await pokeApi.get<PopularResponse>(`/pokemon/${name}`);
  return {
    props: {
      ...pokemonResponseToUIProps(data),
    },
  };
};

export default PokemonByNamePage;
