import { Grid } from "@nextui-org/react";
import { NextPage } from "next";
import { Layout } from "../components/layouts";
import { ItemPokemon } from "../components/pokemon";
import { getAllFavorites } from "../utils";

const Favorites: NextPage = () => {
  return (
    <Layout>
      <Grid.Container justify="flex-start" gap={2}>
        {getAllFavorites().map((pokemon, index) => (
          <ItemPokemon
            key={pokemon.id}
            id={Number(pokemon.id)}
            name={pokemon.name}
            img={pokemon.pokemonImg}
            url={""}
          />
        ))}
      </Grid.Container>
    </Layout>
  );
};

export default Favorites;
