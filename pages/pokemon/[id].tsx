import { Button, Card, Container, Grid, Image, Text } from "@nextui-org/react";
import { GetStaticPaths, GetStaticProps } from "next";
import { Layout } from "../../components/layouts";
import { NextPage } from "next";
import { pokeApi } from "../../api";
import { pokemonResponseToUIProps } from "../../utils";
import { PopularResponse } from "../../interfaces/pokemon-full";
import { PropsMapped } from "../../interfaces/pokemonProps";
import { useEffect } from "react";
import confetti from "canvas-confetti";
import { usePokemonLocalStorage } from "../../hooks";

export const PokemonPage: NextPage<PropsMapped> = ({
  imgBack,
  imgFront,
  imgShintyBack,
  imgShinyFront,
  name,
  pokemonImg,
  id,
}) => {
  const { toggle, isFavoriteState } = usePokemonLocalStorage({
    imgBack,
    imgFront,
    imgShintyBack,
    imgShinyFront,
    name,
    pokemonImg,
    id,
  });

  useEffect(() => {
    if (isFavoriteState)
      confetti({
        zIndex: 999,
        particleCount: 100,
        spread: 160,
        angle: -100,
        origin: {
          x: 1,
          y: 0,
        },
      });
  }, [isFavoriteState]);

  return (
    <Layout title={name}>
      <Grid.Container
        css={{
          marginTop: "5px",
        }}
        gap={2}
      >
        <Grid xs={12} sm={4}>
          <Card
            isHoverable
            isPressable
            css={{
              padding: "30px",
            }}
          >
            <Card.Body>
              <Card.Image
                src={pokemonImg || "/no-image.png"}
                alt={name}
                width="100%"
                height={200}
              />
            </Card.Body>
          </Card>
        </Grid>
        <Grid xs={12} sm={8}>
          <Card>
            <Card.Header
              css={{
                display: "flex",
                justifyContent: "space-between",
              }}
            >
              <Text h1 transform="capitalize">
                {name}
              </Text>
              <Button color="gradient" onPress={toggle}>
                Guardar en favoritos {!isFavoriteState || `❤`}
              </Button>
            </Card.Header>
            <Card.Body>
              <Text size={30}>Sprites:</Text>
              <Container direction="row" display="flex" gap={0}>
                <Image src={imgFront} alt={name} width={100} height={100} />
                <Image src={imgBack} alt={name} width={100} height={100} />
                <Image
                  src={imgShinyFront}
                  alt={name}
                  width={100}
                  height={100}
                />
                <Image
                  src={imgShintyBack}
                  alt={name}
                  width={100}
                  height={100}
                />
              </Container>
            </Card.Body>
          </Card>
        </Grid>
      </Grid.Container>
    </Layout>
  );
};

export const getStaticPaths: GetStaticPaths = async (ctx) => {
  //https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg
  console.log(ctx, "aea");
  const params = Array.from({ length: 151 }).map((_, i) => ({
    params: { id: `${i + 1}` },
  }));
  return {
    paths: params,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { id } = ctx.params as { id: string };
  //https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg
  const { data } = await pokeApi.get<PopularResponse>(`/pokemon/${id}`);
  return {
    props: {
      ...pokemonResponseToUIProps(data),
    },
  };
};

export default PokemonPage;
