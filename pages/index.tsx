import type { GetStaticProps, NextPage } from "next";
import { pokeApi } from "../api";
import { Layout } from "../components/layouts";
import { PokemonListResponse, SmallPokemon } from "../interfaces";
import { getUrlImage } from "../utils";
import { Grid, Image } from "@nextui-org/react";
import { ItemPokemon } from "../components/pokemon";

interface Props {
  pokemons: SmallPokemon[];
}

const HomePage: NextPage<Props> = ({ pokemons }) => {
  return (
    <Layout title="Listado de pokemones">
      <Image src="/img/banner.png" alt="banner" width={200} height={150} />
      <Grid.Container justify="flex-start" gap={2}>
        {pokemons.map((pokemon, index) => (
          <ItemPokemon {...pokemon} key={index} />
        ))}
      </Grid.Container>
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const { data } = await pokeApi.get<PokemonListResponse>("/pokemon?limit=151");
  //https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/132.svg
  const pokemons: SmallPokemon[] = data.results.map((pokemon, index) => ({
    ...pokemon,
    id: index + 1,
    img: getUrlImage(index + 1),
  }));
  return {
    props: {
      pokemons: pokemons,
    },
  };
};

export default HomePage;
