import { useState } from "react";
import { PropsMapped } from "../interfaces/pokemonProps";
import { deleteFavorite, isFavorite, saveLocalStorage } from "../utils";

export function usePokemonLocalStorage(model: PropsMapped) {
  const [isFavoriteState, setIsFavoriteState] = useState(isFavorite(model.id));

  function saveFavorite() {
    saveLocalStorage(model);
    setIsFavoriteState(isFavorite(model.id));
  }

  function deleteItem() {
    deleteFavorite(model.id);
    setIsFavoriteState(isFavorite(model.id));
  }

  function toggle() {
    if (isFavoriteState) {
      return deleteItem();
    }

    return saveFavorite();
  }

  return {
    isFavoriteState,
    toggle,
  };
}
